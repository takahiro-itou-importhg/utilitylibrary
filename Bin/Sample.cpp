//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                      ---  Utility  Library  ---                      **
**                                                                      **
**          Copyright (C), 2001 - 2016, Takahiro Itou                   **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      サンプルプログラム。
**
**      @file       Sample.cpp
**/

#include    <UtilLibs/Common/UtilLibsSettings.h>

#include    <iostream>

int  main(int  argc,  char *  argv[])
{
    return ( 0 );
}
